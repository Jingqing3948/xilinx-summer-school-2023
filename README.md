# 项目介绍

我们组想做一个傅里叶变换后在频域上降噪的音频处理。处理逻辑如下：

1. 接收到时域信号。

2. 时域信号通过 FFT 转为频域信号，形式为：横坐标为频率，纵坐标为实部/虚部。

3. 根据事先设定好的 threshold 对一定阈值范围内的频域信号进行滤波。如本例中设定为0.1阈值，则频率小于最大频率*0.1的部分全部被滤掉。

4. 进行逆向傅里叶变换，输出得到时域信号。

# 项目开发

主要以 lab1 的代码为参考，自己尝试实现音频上傅里叶正逆变换代码，阈值清零反倒是很简单的部分。

只论代码还是比较好写的，但是关键难点还是在于 hls 优化部分。

首先我学习了 for 循环优化的内容，这也是造成我们代码耗时长的短板。我们记录的一些试错经历如下：

对数组进行划分来并行计算：

```c
data_t fftReal[N];
data_t fftImag[N];
#pragma HLS array_partition variable=fftReal complete dim=1
#pragma HLS array_partition variable=fftImag complete dim=1
```

由于仅用于测试案例，我把输入数值固定为常数后嵌套循环变为 perfect loop，如下所示：

```c
for (int k = 0; k < N; ++k) {
        for (int n = 0; n < N; ++n) {
#pragma HLS PIPELINE
            double angle = 2.0 * PI * k * n / N;
            fftReal[k] += x[n] * cos(angle);
            fftImag[k] -= x[n] * sin(angle);
        }
    }
```
内层流水来获得更高的效率；

对于常数赋值等操作完全展开循环：

```c
for(int k=0;k<N;k++){
#pragma HLS UNROLL
	fftReal[k] = 0;
	fftImag[k] = 0;
}
```
等。

#总结

很可惜，由于学艺不精，最终仍然存在一些优化的问题等待解决，最终优化问题如下：interval 过长，吞吐率过低。

![输入图片说明](image.png)

在本次学习经历中学到了很多！也非常感谢xilinx和东南大学提供的这次学习机会。希望以后有机会能再次参加并完成一个项目。
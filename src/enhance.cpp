#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "./enhance.h"


void enhance(data_t *y, data_t *x, int len)
{

#pragma HLS INTERFACE m_axi port=x offset=slave depth=100
#pragma HLS INTERFACE m_axi port=y offset=slave depth=100


#pragma HLS INTERFACE s_axilite port=len  bundle=CTRL
#pragma HLS INTERFACE s_axilite port=return bundle=CTRL
	data_t res;

	data_t fftReal[N];
	data_t fftImag[N];
#pragma HLS array_partition variable=fftReal complete dim=1
#pragma HLS array_partition variable=fftImag complete dim=1

	double tri

	// 设置降噪阈值
	const int threshold = N/10;

	for(int k=0;k<N;k++){
#pragma HLS UNROLL
		fftReal[k] = 0;
		fftImag[k] = 0;
	}

    for (int k = 0; k < N; ++k) {
        for (int n = 0; n < N; ++n) {
#pragma HLS PIPELINE
            double angle = 2.0 * PI * k * n / N;
            fftReal[k] += x[n] * cos(angle);
            fftImag[k] -= x[n] * sin(angle);
        }
    }


	for (int n = 0; n < N; ++n) {
	    double sum = 0.0;
	    for (int k = 0; k < N; ++k) {
#pragma HLS PIPELINE
	        double angle = 2.0 * PI * k * n / N;
	        sum += fftReal[k] * cos(angle);
	        sum -= fftImag[k] * sin(angle);
	    }
	    *y += sum;
        *y /= N;
        y++;
    }
}
